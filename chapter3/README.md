# Chapter 3

- L'annexe A au chapitre 3 correspond au code écrit en Julia qui inclut l'implémentation de la loi gamma généralisée, et le calcul de la log-vraisemblance.
- L'annexe B présente une étude réalisée sur les échantillons de dons de moelle osseuse (Bone Marrow) pour lesquelles on observe une hétérogénéité entre individus. 

