#################################
###      Log-Likelihood      ####
### Model: Generalized Gamma ####
#################################

using Statistics
using Distributions, Random
using SpecialFunctions


#########################
### Generalized Gamma ###
#########################

#Implementation modified from https://github.com/jlapeyre/GeneralizedGammaDistribution.jl
#Where we corrected some mistakes and added an implementation of the cdf

struct GeneralizedGamma <: ContinuousUnivariateDistribution 
    a::Float64
    d::Float64
    p::Float64
end

function GeneralizedGamma(a,d,p)
    GeneralizedGamma(a,d,p)
end

function param_a_towards_mu(a,d,p)
    mu = log(a) + log(d/p)/p
    sig = 1/sqrt(p*d)
    Q = sqrt(p/d)* sign(p)
    
    return mu,sig,Q
    
end

function param_mu_toward_a(mu,sig,Q)
    d = 1/(sig*Q)
    p = Q/sig
    a =abs(Q)^(2 / p) * exp(mu)
    return a,d,p
end

function Distributions.pdf(distr::GeneralizedGamma,x::Real)
    a = distr.a
    d = distr.d
    p = distr.p
    return sign(p)*(p/a^d)/gamma(d/p) * x^(d-1) * exp(-(x/a)^p)
end

function Distributions.cdf(distr::GeneralizedGamma,x::Real)
    a = distr.a
    d = distr.d
    p = distr.p
    d_gamma = Gamma(d/p,1)
    return 1 - cdf(d_gamma, (x/a)^p)
end

# Use the algorithm given in the documentation pages for the R package flexsurv.
function Distributions.rand(distr::GeneralizedGamma)
    mu, sig, Q = param_a_towards_mu(distr.a,distr.d,distr.p)
    Qs = Q^2
    gamma_deviate = rand(Distributions.Gamma(1 / Q^2, 1))  # Approximation: only saves 10 or so percent time.
    w = log(Qs * gamma_deviate) / Q
    return exp(mu + sig * w)
end

function Distributions.mean(distr::GeneralizedGamma)
    mu, sig, Q = param_a_towards_mu(distr.a,distr.d,distr.p)
    Qs = Q^2
    iQs = 1 / Qs
    a = Qs^(sig / Q) * exp(mu)
    return a * Distributions.gamma(iQs + sig / Q) / Distributions.gamma(iQs)
end

function Distributions.var(distr::GeneralizedGamma)
    d2 = Distributions.gamma(distr.d / distr.p)
    return distr.a^2 * ((Distributions.gamma((distr.d + 2) / distr.p) / d2) -
                  (Distributions.gamma((distr.d + 1) / distr.p) / d2)^2)
end

#########################


function loglikelihood(T1, mu, sig, Q)
    a,d,p = param_mu_toward_a(mu, sig, Q)
    distr = GeneralizedGamma(a,d,p)
    value_loglikelihood = 0.0
    max_time = 96
    dt = 1
    
    for i in 1:length(T1)
        if T1[i] == Inf
            value_loglikelihood += log(1 - cdf(distr, max_time))
        else
            value_loglikelihood += log( cdf(distr, T1[i]) - cdf(distr, T1[i]-dt))
        end

    end
    
    return value_loglikelihood
end

