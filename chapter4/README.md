# Chapter 4

- L'annexe A au chapitre 4 correspond à la liste des statistiques descriptives ainsi qu'aux graphes présentant la variations des valeurs prises sur l'ensemble des paramètres échantillonnés par Latin Hypercube
- L'annexe B présente les graphes associés au calcul des indices de Sobol, pour chaque statistique descriptive, suivant si elle est associée à l'expérience Incucyte ou MultiGen
- L'annexe C présent le script écrit en Julia permettant l'estimation des paramètres avec l'algorithme CMA-ES
- L'annexe D présente les étapes ayant permis de faire la sélection des statistiques descriptives

