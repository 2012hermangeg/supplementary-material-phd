# Chapter 6

- L'annexe A au chapitre 6 présente la solution analytique au système d'ODE pour le modèle à quatre compartiments. 
- L'annexe B présente l'article présenté (soumis, accepté avec modifications mineures, non publié) au 2nd International Symposium on Mathematical and Computational Oncology (ISMCO'20). Il s'agit d'une version intermédiaire par rapport au modèle présenté au chapitre 5, qui en particulier est moins parcimonieux.
- L'annexe C présente une extension du modèle présenté au chapitre 6, prenant en compte des mécanismes de régulation (article non soumis)
