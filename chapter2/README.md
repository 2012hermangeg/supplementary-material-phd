# Chapter 2

- L'annexe A au chapitre 2 présente les résultats de la filtration des trails pour chaque souris étudiée
- L'annexe B présente les résultats de la correction des effets batchs
- L'annexe C détaille les résultats concernant les trajectoires d'évolution des intensités des différents marqueurs étudiés

